package CriteriaPattern;
import java.util.ArrayList;

import Person.Person;

public class AgeOneAgeTwoCriteria implements Criteria {
	
	Criteria ageOneCriteria;
	Criteria ageTwoCriteria;
	
	public AgeOneAgeTwoCriteria(Criteria ageOneCriteria, Criteria ageTwoCriteria) {
		this.ageOneCriteria = ageOneCriteria;
		this.ageTwoCriteria = ageTwoCriteria;
	}
	
	public ArrayList<Person> meetCriteria(ArrayList<Person> person) {
		ArrayList<Person> list = new ArrayList<Person>();
		list = ageOneCriteria.meetCriteria(person);
		return ageTwoCriteria.meetCriteria(list);
	}

}
