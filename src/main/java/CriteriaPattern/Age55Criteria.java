package CriteriaPattern;

import java.util.ArrayList;

import Person.Person;

public class Age55Criteria implements Criteria {
	public ArrayList<Person> meetCriteria(ArrayList<Person> person) {
		ArrayList<Person> list = new ArrayList<Person>();
		for(Person p : person) {
			if(p.getAge() > 55) {
				list.add(p);
			}
		}
		return list;
	}
}
