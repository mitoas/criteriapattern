package CriteriaPattern;
import java.util.ArrayList;

import Person.Person;

public class AgeAndGenderCriteria implements Criteria {
	
	Criteria ageCriteria;
	Criteria genderCriteria;
	
	public AgeAndGenderCriteria(Criteria ageCriteria, Criteria genderCriteria) {
		this.ageCriteria = ageCriteria;
		this.genderCriteria = genderCriteria;
		
	}
	
	public ArrayList<Person> meetCriteria(ArrayList<Person> person) {
		ArrayList<Person> list = new ArrayList<Person>();
		list = ageCriteria.meetCriteria(person);
		return (genderCriteria.meetCriteria(list));
	}

}
