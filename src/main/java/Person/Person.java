package Person;

public class Person {
	
	String gender;
	int age;
	
	public Person(String gender, int age) {
		this.gender = gender;
		this.age = age;
	}
	
	public String getGender() {
		return this.gender;
	}
	
	public int getAge() {
		return this.age;
	}

}
